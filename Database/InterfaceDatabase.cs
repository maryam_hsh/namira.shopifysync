﻿using System.Collections.Generic;
using System.Data;

namespace ShopifySync.Database
{
    public class InterfaceDatabase : BaseDatabase
    {
        public InterfaceDatabase()
        {
            SettingLoader settingLoader = new SettingLoader();
            Open(settingLoader.Setting.Interface);
        }
        public List<SyncObjectsModel> GetSyncObjects()
        {
            DataTable t = Get("select");
            return new List<SyncObjectsModel>();
        }
    }
}
