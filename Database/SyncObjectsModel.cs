﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Xml.Linq;

namespace ShopifySync.Database
{
    public class SyncObjectsModel
    {
        public string ObjectName { get; set; }
        public string LocalTableName { get; set; }
        public string DocumentURL { get; set; }
        public bool SyncDirectionUp { get; set; }
        public bool SyncDirectionDown { get; set; }
        public int SyncIntervalSeconds { get; set; }
        public DateTime LastSyncTimestamp { get; set; }
        public SyncResult.SyncResult LastSyncResult { get; set; }
    }
}
