﻿namespace ShopifySync.Database
{
    public class EcommerceDatabase : BaseDatabase
    {
        public EcommerceDatabase()
        {
            SettingLoader settingLoader = new SettingLoader();
            Open(settingLoader.Setting.Ecommerce);
        }
    }
}
