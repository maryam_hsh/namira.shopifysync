﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ShopifySync.Database
{
    public abstract class BaseDatabase
    {
        private Logger logger;
        protected SqlConnection connection;
        public BaseDatabase()
        {
            this.logger = new Logger();
        }
        ~BaseDatabase()
        {
            try
            {
                connection.Close();
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }
        }
        protected void Open(DatabaseSetting setting)
        {
            try
            {
                connection = new SqlConnection(setting.GetConnectionString());
                connection.Open();
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }
        }
        protected DataTable Get(string Query)
        {
            try
            {
                SqlCommand command = new SqlCommand(Query, connection);
                SqlDataReader reader = command.ExecuteReader();
                DataTable data = new DataTable();
                data.Load(reader);
                return data;
            }
            catch(Exception ex)
            {
                logger.Log(ex);
                return new DataTable();
            }
        }
        protected void Update(string Query) {
            try
            {
                SqlCommand command = new SqlCommand(Query, connection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }
        }
    }
}
