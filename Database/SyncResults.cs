﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Xml.Linq;

namespace ShopifySync.Database
{
    public static class SyncResult
    {
        public static enum SyncResult
        {
            OK,
            ERROR,
            SYNCING
        }
    }
}
