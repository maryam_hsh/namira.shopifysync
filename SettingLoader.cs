﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace ShopifySync
{
    public class SettingLoader
    {
        private Logger logger;
        public string FileFullPath { get { return System.AppContext.BaseDirectory + "setting.json"; } }
        public Setting Setting { get; private set; } = new Setting();
        //Initialize
        public SettingLoader()
        {
            this.logger = new Logger();
            Load();
        }
        // Read Json File
        private void Load()
        {
            try
            {
                if (File.Exists(FileFullPath))
                {
                    var jsonString = File.ReadAllText(FileFullPath);
                    this.Setting = JsonConvert.DeserializeObject<Setting>(jsonString);
                }
                else
                {
                    Save();
                    logger.Log("New setting file was generated: " + FileFullPath);
                }
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }
        }
        //Write Json File
        public void Save()
        {
            try
            {
                var jsonString = JsonConvert.SerializeObject(this.Setting);
                File.WriteAllText(this.FileFullPath, jsonString);
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }
        }
    }
}
