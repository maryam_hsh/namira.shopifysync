﻿namespace ShopifySync
{
    public class DatabaseSetting
    {
        public string IP { get; set; } = "127.0.0.1";
        public string Port { get; set; } = "1433";
        public string Name { get; set; } = "";
        public string User { get; set; } = "";
        public string Pass { get; set; } = "";
        public string GetConnectionString()
        {
            return @"Data Source=" + IP + "," + Port + ";Initial Catalog=" + Name + ";User ID=" + User + ";Password=" + Pass;
        }
    }
}
