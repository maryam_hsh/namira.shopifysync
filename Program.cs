﻿using ShopifySync.Database;
using System;

namespace ShopifySync
{
    public class Program
    {
        static void Main()
        {
            Logger loggerTest = new Logger();
            SettingLoader Setting = new SettingLoader();
            try
            {
                loggerTest.Log("-------------------------------" + DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss") + "-------------------------------");
                loggerTest.Log("Execution was started.");

                EcommerceDatabase ecommerceDatabase = new EcommerceDatabase();
            }
            catch (Exception ex)
            {
                loggerTest.Log(ex);
            }
            loggerTest.Log("Execution was finished.");
            loggerTest.Log("---------------------------------------------------------------------------------");
            loggerTest.Log("");
        }
    }
}
