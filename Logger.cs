﻿using System;
using System.IO;

namespace ShopifySync
{
    public class Logger
    {
        public void Log(Exception ex)
        {
            Log("An error has occured: " + ex.Message);
            Log("\tStack: " + ex.StackTrace);
        }
        public void Log(string message)
        {
            try
            {
                string fileName = GetFileName();
                using (StreamWriter fs = new StreamWriter(File.Open(fileName, FileMode.Append)))
                {
                    fs.WriteLine(message);
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }
        private string GetFileName()
        {
            return $"ShopifySync-{DateTime.Now.Year}{DateTime.Now.Month.ToString().PadLeft(2, '0')}{DateTime.Now.Day.ToString().PadLeft(2, '0')}.txt";
        }
    }
}
